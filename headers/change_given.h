#include <math.h>
#include <iostream>

namespace Change {
  static int *calculate_change(const float cost, const float change_given) {

    int change[11] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
    int coins[11] = { 2000, 1000, 500, 200, 100, 50, 20, 10, 5, 2, 1 };

    int change_needed = (change_given - cost) * 100;

    for(unsigned i = 0; i < sizeof(change)/sizeof(change[0]); i++) {

      change[i] += ((change_needed - (change_needed % coins[i])) / coins[i]);
      change_needed = change_needed % coins[i];

      std::cout << "Number of £" << (float)coins[i] / 100 << ": " << change[i] << std::endl;
    }

    return change;

  }
}
