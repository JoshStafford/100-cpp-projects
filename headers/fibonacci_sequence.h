#include <iostream>

namespace Fibonacci {

  static void calculate_fibonacci(const unsigned n) {

      unsigned long val1 = 0, val2 = 1, val;

      std::cout << val1 << std::endl;

      for(unsigned i = 1; i < n; ++i) {

        val = val1+val2;

        val1 = val2;
        val2 = val;

        std::cout << val << std::endl;

      }

  }

}
