#include <iostream>
#include <math.h>

namespace Primes {

  static void calculate_prime_factors(unsigned num) {

    while(num%2 == 0) {
      std::cout << 2 << std::endl;
      num = num*0.5;
    }

    for(unsigned i = 3; i <= sqrt(num); i+=2) {
      while(num%i == 0) {
        std::cout << i << std::endl;
        num = num/i;
      }
    }

    if(num > 2) {
      std::cout << num << std::endl;
    }

  }
}
