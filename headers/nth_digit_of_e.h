namespace eCalculator {

  static inline unsigned long factorial(const unsigned long digit) {

    long double total = digit;

    for(unsigned i = digit-1; i > 0; i--) {

      total *= i;

    }

    return total;

  }

  static long double calculate_e(const unsigned n) {

    long double e = 1;

    for(unsigned i = 1; i < n; ++i) {

      e += 1.0 / (long double)factorial(i);

    }

    return e;

  }

}
