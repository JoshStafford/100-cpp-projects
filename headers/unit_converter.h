#include <string>
#include <map>
#include <iostream>

namespace Convert {

	// Function convert() requires three parameters:
	// First two parameters are unsigned values that are associated with the units below starting from 1.
	// The first value is the unit to convert from and the second value is the unit to convert to.
	// Third parameter is the float value that they wish to convert.

	std::string units[4] = { "kilometre", "metre", "foot", "inch" };

	std::map<std::string, float>::iterator it1 ; 

	std::map<std::string, float> conversions = {
		{ "kilometre-metre", 1000.0 },
		{ "metre-kilometre", 0.001 },
		{ "foot-metre", 0.3048 },
		{ "metre-foot", 3.28083989501 },
		{ "inch-foot", 0.0833333 },
		{ "foot-inch", 12.0000048 },
		{ "metre-inch", 39.3701 },
		{ "inch-metre", 0.02539998628 }
	};

	inline static std::string create_conversion(const unsigned op1, const unsigned op2){

		std::string phrase1 = units[op1-1];
		std::string phrase2 = units[op2-1];

		return phrase1 + "-" + phrase2;

	}

	static float convert(const unsigned op1, const unsigned op2, const float val) {

		std::string conversion_text = create_conversion(op1, op2);

		it1 = conversions.find(conversion_text); 
      
	    if(it1 != conversions.end()) 
	        return val * it1->second; 
	}

}