#include <string>
#include <math.h>

namespace Calculator {

	static std::string calc(const char &op, const float a, const float b) {

		switch(op){

			case '*': return std::to_string(a) + " x " + std::to_string(b) + " = " + std::to_string(a*b);
			case '+': return std::to_string(a) + " + " + std::to_string(b) + " = " + std::to_string(a+b);
			case '-': return std::to_string(a) + " - " + std::to_string(b) + " = " + std::to_string(a-b);
			case '/': return std::to_string(a) + " / " + std::to_string(b) + " = " + std::to_string(a/b);
			case '^': return std::to_string(a) + " ^ " + std::to_string(b) + " = " + std::to_string(pow(a, b));
			default: return "Operator not supported.";
			
		}

	}

}