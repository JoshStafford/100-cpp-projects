#include <string>
#include <thread>
#include <chrono>

namespace Alarm {

	// Function countdown() requires two parameters:
	// The first parameter is the unit of time (being s, m or h for seconds, minutes or hours).
	// The second parameter is the length of time.


	static void countdown(const char unit, const float &time) {

		float t;

		switch(unit){

			case 's': t = time; break;
			case 'm': t = 60*time; break;
			case 'h': t = 3600*time; break;

		}

		std::cout << "Countdown for " << t << " seconds has begun..." << std::endl;

		for(unsigned i = 0; i < t; ++i){
			std::cout << ">> " << t-i << " <<" << std::endl;
			std::this_thread::sleep_for (std::chrono::seconds(1));
		}

		std::cout << "Countdown has ended." << std::endl;

	}


}