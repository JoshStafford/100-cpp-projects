#include <iostream>
#include <string>
#include <math.h>

inline static bool check_next() {

  std::string choice;
  std::getline(std::cin, choice);

  if(choice == "n"){
    return false;
  } else {
    return true;
  }

}

static void calculate_next_prime(unsigned val = 2) {

  std::cout << "Hit enter to find the next prime or type n to quit." << std::endl << std::endl;
  unsigned divisions;
  //
  // if()
  // std::cout << "Prime: " << 2 << std::endl;

  while(true) {

    if(val != 2 && val%2==0) { val+=1; }

    if(!check_next()) { break; }

    while(true) {
      divisions=0;

      for(unsigned i = 3; i <= sqrt(val); i+=2) {
        if(val%i==0 && val != i) { divisions+=1; break; }
      }

      if(divisions == 0) {
        break;
      }

      val += 2;
    }

    std::cout << "Prime: " << val << std::endl;
    if(val==2) { val -= 1; }
    val+=2;
  }
}
