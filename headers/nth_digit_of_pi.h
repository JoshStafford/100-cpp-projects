#include "math.h"
#include <iostream>
#include <string>

namespace piCalculator {

  static long double calculate_pi(const unsigned n) {

    long double pi = 0;

    long double a = 1.0;
    long double b = 1.0 / sqrt(2);
    long double t = 0.25;
    long double p = 1.0;

    long double _a;
    long double _b;
    long double _t;
    long double _p;

    for(unsigned i = 0; i < n; i++) {

      _a = (a+b) / 2;
      _b = sqrt((a*b));
      _t = t - p*(pow(a-_a, 2));
      _p = 2*p;

      a = _a;
      b = _b;
      t = _t;
      p = _p;

    }

    pi = ((a+b)*(a+b)) / (4*t);

    return pi;

  }

}
