#include <string>
#include <math.h>
#include <iostream>

namespace binDen {

	static unsigned binary_to_denary(const std::string &b) {

		unsigned conversion = 0;

		for(int i = 7; i >= 0; i--) {
			if(b[i] == '1'){
				// std::cout << i << std::endl;
				conversion += std::pow(2, 7-i);
				// std::cout << conversion << std::endl;
			}
		}

		return conversion;

	}

	static std::string denary_to_binary(const unsigned d) {

		std::string conversion = "";
		unsigned vals[8] = { 128, 64, 32, 16, 8, 4, 2, 1 };
		unsigned val = d;

		for(unsigned i = 0; i < 8; i++) {
			if(val >= vals[i]) {
				conversion += "1";
				val -= vals[i];
			} else {
				conversion += "0";
			}
		}

		std::cout << conversion << std::endl;

	}

}