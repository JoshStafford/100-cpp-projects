#pragma once

//Challenge files
#include "headers/nth_digit_of_pi.h"
#include "headers/nth_digit_of_e.h"
#include "headers/fibonacci_sequence.h"
#include "headers/prime_factors.h"
#include "headers/next_prime.h"
#include "headers/change_given.h"
#include "headers/binary_denary.h"
#include "headers/calculator.h"
#include "headers/unit_converter.h"
#include "headers/alarm_clock.h"


int main() {

	char unit;
	float time;
	std::cin >> unit;
	std::cin >> time;

	Alarm::countdown(unit, time);

	return 0;
}
